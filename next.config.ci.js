// @ts-check

const isProd = process.env.NODE_ENV === 'production';

/**
 * @type {import('next/dist/next-server/server/config').NextConfig}
 */
const nextConfig = {
  // Use the CDN in production and localhost for development.
  assetPrefix: isProd ? 'https://keithyeh.gitlab.io/candlestick-chart-practice' : '',
};

module.exports = nextConfig;
