# candlestick-chart-practice

Use [D3.js](https://www.npmjs.com/package/d3) to implement a [Candlestick chart](https://en.wikipedia.org/wiki/Candlestick_chart)

[Live Demo](https://keithyeh.gitlab.io/candlestick-chart-practice)

## Get Started

```
yarn install
yarn start
```
