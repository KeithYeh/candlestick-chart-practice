module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
    'react-hooks',
  ],
  settings: {
    'import/resolver': {
      node: true,
      'babel-module': {},
      'eslint-import-resolver-typescript': true,
    },
  },
  extends: [
    'next',
    'airbnb',
    'plugin:import/typescript',
  ],
  rules: {
    'max-len': 'off',
    'no-multi-spaces': 'off',
    'no-plusplus': [
      'error',
      {
        allowForLoopAfterthoughts: true,
      },
    ],
    'no-redeclare': 'off',
    'no-shadow': 'off',
    'no-undef': 'off', // Typescript will inspect this. Turn off this to prevent unused var in interface
    'no-unused-vars': 'off',
    'no-use-before-define': 'off',

    'import/extensions': [
      'error',
      'never',
      {
        json: 'always',
        jsx: 'never',
        ts: 'never',
        tsx: 'never',
      },
    ],
    // @see https://basarat.gitbooks.io/typescript/content/docs/tips/defaultIsBad.html
    'import/prefer-default-export': 'off',
    // For backend class properties
    'lines-between-class-members': 'off',

    'react/prop-types': 'off', // We use typescript
    'react/jsx-filename-extension': [
      'error',
      {
        extensions: [
          '.jsx',
          '.tsx',
        ],
      },
    ],
    'react/jsx-props-no-spreading': 'off',
    'react/require-default-props': ['error', { ignoreFunctionalComponents: true }],

    // TypeScript rules
    '@typescript-eslint/adjacent-overload-signatures': 'error',
    '@typescript-eslint/array-type': 'error',
    '@typescript-eslint/ban-types': 'error',
    '@typescript-eslint/consistent-type-assertions': [
      'error',
      {
        assertionStyle: 'as',
        objectLiteralTypeAssertions: 'allow',
      },
    ],
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-member-accessibility': 'off',
    '@typescript-eslint/member-delimiter-style': 'error',
    '@typescript-eslint/member-ordering': 'error',

    '@typescript-eslint/naming-convention': [
      'error',
      {
        selector: 'class',
        format: ['PascalCase'],
      },
      {
        selector: 'interface',
        format: ['PascalCase'],
        custom: {
          regex: '^I[A-Z]',
          match: false,
        },
      },
    ],

    '@typescript-eslint/no-array-constructor': 'error',
    '@typescript-eslint/no-misused-new': 'error',
    '@typescript-eslint/no-non-null-assertion': 'error',
    '@typescript-eslint/no-redeclare': 'error',
    '@typescript-eslint/no-shadow': 'error',
    '@typescript-eslint/no-unused-vars': 'error',
    '@typescript-eslint/no-use-before-define': 'error',
    '@typescript-eslint/triple-slash-reference': [
      'error',
      {
        path: 'never',
        types: 'never',
        lib: 'never',
      },
    ],
  },
};
