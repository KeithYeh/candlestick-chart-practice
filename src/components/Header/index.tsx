import React from 'react';
import styled from 'styled-components';

import { ColorPicker } from '@app/components/ColorPicker';
import { useData, useTheme } from '@app/hooks';

import { DrawingSwitch } from './DrawingSwitch';
import { IntervalSelector } from './IntervalSelector';

const Root = styled.header`
  display: flex;
  position: sticky;
  top: 0;
  left: 0;
  margin-bottom: 1rem;
  padding: 0 .5rem;
  height: ${(props) => props.theme.headerHeight};
  align-items: center;
  background-color: #fff;
  box-shadow: 0 1px 4px rgba(0, 0, 0, .25);
  
  > *:not(:last-child) {
    margin-right: 1rem;
  }
`;

const volumesSwitchId = 'volume-switch';

export function Header() {
  const { actions: dataActions, state: dataState } = useData();
  const { actions: themeActions, state: themeState } = useTheme();

  return (
    <Root>
      <h1>My Stock</h1>
      <IntervalSelector />
      <label htmlFor={volumesSwitchId}>
        <input
          id={volumesSwitchId}
          type="checkbox"
          checked={dataState.showVolumes}
          onChange={(e) => dataActions.updateShowVolumes(e.target.checked)}
        />
        {' '}
        Show Volumes
      </label>
      <ColorPicker
        color={themeState.bullColor}
        onChange={(color) => themeActions.updateBullColor(color.hex)}
        label="Bull Candles"
      />
      <ColorPicker
        color={themeState.bearColor}
        onChange={(color) => themeActions.updateBearColor(color.hex)}
        label="Bear Candles"
      />
      <DrawingSwitch />
      <button
        type="button"
        onClick={() => dataActions.updateRectangles([])}
      >
        Clear Rectangles
      </button>
      <button
        type="button"
        onClick={() => dataActions.regeneratePoints({ interval: dataState.interval })}
      >
        Regenerate Data
      </button>
    </Root>
  );
}
