import React, { ChangeEvent } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock } from '@fortawesome/free-regular-svg-icons';

import { useData } from '@app/hooks';
import { Interval } from '@app/models';

const intervalSelectorId = 'interval-selector';
const ClockIcon = styled(FontAwesomeIcon)`
  margin-right: .5rem;
`;

export function IntervalSelector() {
  const { actions, state } = useData();

  const handleChange = (e: ChangeEvent<HTMLSelectElement>) => {
    const interval = parseInt(e.target.value, 10);
    actions.updateInterval(interval);
    actions.regeneratePoints({ interval });
  };

  return (
    <label htmlFor={intervalSelectorId}>
      <ClockIcon icon={faClock} />
      <select id={intervalSelectorId} value={state.interval} onChange={handleChange}>
        <option value={Interval.ONE_MINUTE}>1 Minute</option>
        <option value={Interval.FIVE_MINUTES}>5 Minutes</option>
        <option value={Interval.FIFTEEN_MINUTES}>15 Minutes</option>
        <option value={Interval.THIRTY_MINUTES}>30 Minutes</option>
        <option value={Interval.AN_HOUR}>1 Hour</option>
        <option value={Interval.TWO_HOURS}>2 Hours</option>
        <option value={Interval.FOUR_HOURS}>4 Hours</option>
        <option value={Interval.ONE_DAY}>1 Day</option>
        <option value={Interval.ONE_WEEK}>1 Week</option>
        <option value={Interval.ONE_MONTH}>1 Month</option>
      </select>
    </label>
  );
}
