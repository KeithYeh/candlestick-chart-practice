import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faVectorSquare } from '@fortawesome/free-solid-svg-icons';

import { useData } from '@app/hooks';

const drawingSwitchId = 'drawing-switch';

const Root = styled.label<{ checked: boolean }>`
  display: flex;
  width: 1.5rem;
  height: 1.5rem;
  background-color: ${(props) => (props.checked ? '#8D95F1' : 'transparent')};
  align-items: center;
  justify-content: center;
  border: 1px solid #1624B6;
  border-radius: 2px;
  cursor: pointer;
  
  > input {
    display: none;
  }

  input ~ svg {
    color: #1624B6;
  }
  
  input:checked ~ svg {
    color: #fff;
  }
`;

export function DrawingSwitch() {
  const { actions, state } = useData();

  return (
    <Root checked={state.drawing}>
      <input
        id={drawingSwitchId}
        type="checkbox"
        checked={state.drawing}
        onChange={(e) => actions.updateDrawing(e.target.checked)}
      />
      <FontAwesomeIcon icon={faVectorSquare} />
    </Root>
  );
}
