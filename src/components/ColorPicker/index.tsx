import React, { useState } from 'react';
import styled from 'styled-components';
import { BlockPicker, ColorChangeHandler } from 'react-color';

type Props = {
  color: string;
  onChange: ColorChangeHandler;
  label?: string;
};

const Wrapper = styled.div`
  display: flex;
  align-items: center;
`;

const ColorWrapper = styled.div`
  position: relative;
  width: 1.5rem;
  height: 1.5rem;
  padding: 3px;
  border: 1px solid #ccc;
  border-radius: 2px;
  background-color: #fff;
  cursor: pointer;
`;

const CurrentColor = styled.div<Pick<Props, 'color'>>`
  width: 100%;
  height: 100%;
  border-radius: 2px;
  background-color: ${(props) => props.color};
`;

const ColorLabel = styled.span`
  margin-left: .5rem;
`;

const Popover = styled.div`
  position: absolute;
  top: 100%;
  z-index: 2;
`;

const Backdrop = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

export function ColorPicker(props: Props) {
  const { color, onChange, label } = props;
  const [showPicker, setShowPicker] = useState(false);

  const toggleColorPicker = () => {
    setShowPicker(!showPicker);
  };

  return (
    <Wrapper>
      <ColorWrapper>
        <CurrentColor color={color} onClick={() => toggleColorPicker()} />
      </ColorWrapper>
      {label && (<ColorLabel>{label}</ColorLabel>)}
      {showPicker && (
        <Popover>
          <Backdrop onClick={() => setShowPicker(false)} />
          <BlockPicker
            color={color}
            onChangeComplete={onChange}
          />
        </Popover>
      )}
    </Wrapper>
  );
}
