import React from 'react';
import styled from 'styled-components';

type Props = {
  children: NonNullable<React.ReactNode>;
};

const Container = styled.div`
  height: calc(100vh - ${(props) => props.theme.headerHeight} - ${(props) => props.theme.footerHeight} - 2rem);
  margin: 1rem 0;
`;

export function ChartContainer(props: Props) {
  const { children } = props;

  return (
    <Container>{children}</Container>
  );
}
