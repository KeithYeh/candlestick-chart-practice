import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';

import { useData, useTheme } from '@app/hooks';
import { renderChart } from './renderChart';

const Chart = styled.svg`
  width: 100%;
  height: 100%;

  path.domain,
  .tick > line {
    color: rgba(0, 0, 0, .1);
  }

  .tooltip {
    visibility: hidden;
    font-weight: 300;
  }

  &:not(.drawing) {
    .data-point:hover {
      > .bg-highlight {
        stroke-opacity: 0.5;
      }

      .tooltip {
        visibility: visible;
      }
    }
  }

  .ugc-rect {
    stroke: #2ccce4;
    fill: #2ccce4;
    fill-opacity: .1;
  }
`;

export function CandlestickChartSvg() {
  const { actions: dataActions, state: data } = useData();
  const { state: theme } = useTheme();
  const svgRef = useRef<SVGSVGElement>(null);

  const reRenderChart = () => {
    renderChart({
      data,
      dataActions,
      svgRef,
      theme,
    });
  };

  useEffect(() => {
    reRenderChart();
  }, [data, svgRef, theme]); // eslint-disable-line

  useEffect(() => {
    window.addEventListener('resize', reRenderChart);

    return () => window.removeEventListener('resize', reRenderChart);
  });

  return (
    <Chart ref={svgRef}>
      <clipPath id="clip" />
    </Chart>
  );
}
