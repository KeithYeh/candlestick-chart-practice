import * as d3 from 'd3';
import { Rectangle } from '@app/models';
import { RectangleDrawer } from './RectangleDrawer';
import { SVGElementSelection } from './types';

export function enableDrawing(svg: SVGElementSelection, padding: number, addRect: (rectangle: Rectangle) => void) {
  const drawer = new RectangleDrawer(svg, padding);

  svg.call(
    d3.drag<SVGSVGElement, unknown>()
      .on('start', (event) => drawer.onDragStart(event))
      .on('drag', (event) => drawer.onDragging(event))
      .on('end', () => drawer.onDragEnd(addRect)),
  );
}

export function disableDrawing(svg: SVGElementSelection) {
  svg.on('.drag', null);
}
