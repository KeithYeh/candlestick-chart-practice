import * as d3 from 'd3';
import {
  DateAxis,
  DateScaleTime,
  NumberAxis,
  NumberScaleLinear,
} from './types';

type Axes = {
  xAxis: DateAxis;
  yAxis: NumberAxis;
};

type Options = {
  innerHeight: number;
  innerWidth: number;
  xScale: DateScaleTime;
  yScale: NumberScaleLinear;
  zoomScale: number;
};

const getResponsiveTickWidth = (containerSize: number, zoomScale: number): number => {
  let baseTickWidth: number;

  if (containerSize > 1440) {
    baseTickWidth = 60;
  } else if (containerSize > 1024) {
    baseTickWidth = 30;
  } else {
    baseTickWidth = 45;
  }

  return baseTickWidth * zoomScale;
};

export function prepareAxes(options: Options): Axes {
  const {
    innerHeight,
    innerWidth,
    xScale,
    yScale,
    zoomScale,
  } = options;

  const xAxis = d3
    .axisBottom(xScale)
    .scale(xScale)
    .ticks(Math.ceil(innerWidth / getResponsiveTickWidth(innerWidth, zoomScale)))
    .tickSize(-innerHeight) as DateAxis;

  const yAxis = d3
    .axisLeft(yScale)
    .scale(yScale)
    .ticks(Math.ceil(innerHeight / getResponsiveTickWidth(innerHeight, zoomScale)))
    .tickSize(-innerWidth)
    .tickFormat((price) => d3.format('$,')(price)) as NumberAxis;

  return { xAxis, yAxis };
}
