import * as d3 from 'd3';
import { State as Theme } from '@app/context/Theme';
import { Point } from '@app/models';

import {
  DateScaleTime,
  GElementSelection,
  NumberScaleLinear,
} from './types';

type Options = {
  dataLayer: GElementSelection;
  innerHeight: number;
  innerWidth: number;
  points: Point[];
  showVolumes: boolean;
  theme: Theme;
  volumeScale: NumberScaleLinear;
  xScale: DateScaleTime;
  yScale: NumberScaleLinear;
  zoomScale: number;
};

const getResponsiveStrokeWidth = (containerSize: number, zoomScale: number): number => {
  let baseTickWidth: number;

  if (containerSize > 1440) {
    baseTickWidth = 6;
  } else if (containerSize > 1024) {
    baseTickWidth = 4;
  } else {
    baseTickWidth = 3;
  }

  return baseTickWidth * zoomScale;
};

export function renderDataPoints(options: Options) {
  const {
    dataLayer,
    innerHeight,
    innerWidth,
    points,
    showVolumes,
    theme,
    volumeScale,
    xScale,
    yScale,
    zoomScale,
  } = options;

  // helpers
  const formatChange = (open: number, close: number): string => {
    const f = d3.format('+.2%');
    return f((close - open) / open);
  };

  const tint = (p: Point): string => {
    if (p.open === p.close) return '#999';

    return p.open > p.close
      ? theme.bearColor
      : theme.bullColor;
  };

  const strokeWidth = getResponsiveStrokeWidth(innerWidth, zoomScale);

  dataLayer.selectAll('g').remove();

  const dataPoints = dataLayer
    .selectAll('g')
    .data(points)
    .enter()
    .append('g')
    .attr('class', 'data-point')
    .attr('transform', (p) => `translate(${xScale(p.time)}, 0)`);

  // Draw background highlight
  dataPoints.append('line')
    .attr('class', 'bg-highlight')
    .attr('y1', 0)
    .attr('y2', innerHeight)
    .attr('stroke', '#ccc')
    .attr('stroke-width', strokeWidth * 1.5)
    .attr('stroke-opacity', '0');

  // Draw shadow line
  dataPoints.append('line')
    .attr('y1', (p) => yScale(p.high))
    .attr('y2', (p) => yScale(p.low))
    .attr('stroke', tint);

  // Draw candle body
  dataPoints.append('line')
    .attr('y1', (p) => yScale(p.open))
    .attr('y2', (p) => yScale(p.close))
    .attr('stroke-width', strokeWidth)
    .attr('stroke', tint);

  // Data detail
  dataPoints
    .append('title')
    .text((p) => `
${p.time.toLocaleString()}
Open: ${p.open.toFixed(2)}
Close: ${p.close.toFixed(2)} (${formatChange(p.open, p.close)})
Low: ${p.low.toFixed(2)}
High: ${p.high.toFixed(2)}
Volume: ${p.volume}
`);

  // Data tooltip
  dataPoints
    .append('text')
    .attr('class', 'tooltip')
    .attr('transform', (p) => `translate(-${Math.abs(xScale(p.time) - 10)}, 20)`)
    .text((p) => `O: ${p.open.toFixed(2)} | C: ${p.close.toFixed(2)} | L: ${p.low.toFixed(2)} | H: ${p.high.toFixed(2)} | Volume: ${p.volume}`);

  if (showVolumes) {
    dataPoints
      .append('line')
      .attr('y1', (p) => volumeScale(p.volume))
      .attr('y2', innerHeight)
      .attr('stroke-width', strokeWidth)
      .attr('stroke', tint)
      .attr('stroke-opacity', '0.2');
  }
}
