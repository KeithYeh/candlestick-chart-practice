import { Rectangle } from '@app/models';
import {
  RectElementSelection,
  SVGElementDragEvent,
  SVGElementSelection,
} from './types';

export class RectangleDrawer {
  private svg: SVGElementSelection;
  private padding: number;
  private rectangle: RectElementSelection | null;
  private x0: number;
  private y0: number;

  public constructor(svg: SVGElementSelection, padding: number) {
    this.svg = svg;
    this.padding = padding;
    this.rectangle = null;
    this.x0 = 0;
    this.y0 = 0;
  }

  public onDragStart(event: SVGElementDragEvent): void {
    this.x0 = event.x - this.padding;
    this.y0 = event.y - this.padding;

    this.rectangle = this.svg
      .select('.ugc-rect-layer')
      .append('rect')
      .attr('class', 'ugc-rect')
      .attr('x', this.x0)
      .attr('y', this.y0)
      .attr('width', 1)
      .attr('height', 1);
  }

  public onDragging(event: SVGElementDragEvent): void {
    if (!this.rectangle) return;

    const x1 = event.x - this.padding;
    const y1 = event.y - this.padding;

    this.rectangle
      .attr('x', Math.min(this.x0, x1))
      .attr('y', Math.min(this.y0, y1))
      .attr('width', Math.abs(this.x0 - x1))
      .attr('height', Math.abs(this.y0 - y1));
  }

  public onDragEnd(addRect: (rectangle: Rectangle) => void): void {
    if (this.rectangle) {
      const rect = {
        x: +this.rectangle.attr('x'),
        y: +this.rectangle.attr('y'),
        width: +this.rectangle.attr('width'),
        height: +this.rectangle.attr('height'),
      };

      addRect(rect);
    }

    this.rectangle = null;
    this.x0 = 0;
    this.y0 = 0;
  }
}
