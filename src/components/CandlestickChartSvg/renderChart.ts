import { RefObject } from 'react';
import * as d3 from 'd3';
import { Actions as DataActions, State as Data } from '@app/context/Data';
import { State as Theme } from '@app/context/Theme';

import { disableDrawing, enableDrawing } from './drawing';
import { prepareAxes } from './prepareAxes';
import { renderDataPoints } from './renderDataPoints';
import { renderRectangles } from './renderRectangles';
import { SVGElementZoomEvent } from './types';
import { disableZooming, enableZooming } from './zooming';

type Options = {
  data: Data;
  dataActions: DataActions;
  svgRef: RefObject<SVGSVGElement>;
  theme: Theme;
};

export function renderChart(options: Options) {
  const {
    data,
    dataActions,
    svgRef,
    theme,
  } = options;

  if (!svgRef.current) return;

  const {
    drawing,
    points,
    rectangles,
    showVolumes,
  } = data;

  const { width, height } = svgRef.current.getBoundingClientRect();
  const padding = 50;
  const innerWidth = width - (padding * 2);
  const innerHeight = height - (padding * 2);
  const svg = d3.select(svgRef.current);

  if (drawing) {
    svgRef.current.classList.add('drawing');
  } else {
    svgRef.current.classList.remove('drawing');
  }

  // Redraw
  svg.selectAll('g').remove();
  svg.select('#clip').selectAll('rect').remove();

  // Add a clipPath: everything out of this area won't be drawn.
  svg.select('#clip')
    .append('rect')
    .attr('width', innerWidth)
    .attr('height', innerHeight)
    .attr('x', 0)
    .attr('y', 0);

  // Prepare layers
  const rootLayer = svg.append('g').attr('transform', `translate(${padding} ${padding})`);
  const axisLayer = rootLayer.append('g').attr('class', 'axis-layer');
  const xAxisLayer = axisLayer.append('g').attr('transform', `translate(${0}, ${innerHeight})`);
  const yAxisLayer = axisLayer.append('g');
  const ugcRectLayer = rootLayer.append('g').attr('class', 'ugc-rect-layer').attr('clip-path', 'url(#clip)');
  const dataLayer = rootLayer.append('g')
    .attr('class', 'data-layer')
    .attr('clip-path', 'url(#clip)');

  const xExtent = d3.extent(points, (p) => p.time);
  const yExtent = [d3.min(points, (p) => p.low), d3.max(points, (p) => p.high)];
  const volumeExtent = [0, d3.max(points, (p) => p.volume)];

  // @ts-ignore
  const xScale = d3.scaleTime().domain(xExtent).range([0, innerWidth]).nice();
  // @ts-ignore
  const yScale = d3.scaleLinear().domain(yExtent).range([innerHeight, 0]).nice();
  const volumeScale = d3.scaleLinear().domain(volumeExtent as Iterable<number>).range([innerHeight, innerHeight * 0.85]);

  const { xAxis, yAxis } = prepareAxes({
    innerHeight,
    innerWidth,
    xScale,
    yScale,
    zoomScale: 1,
  });

  xAxisLayer.call(xAxis);
  yAxisLayer.call(yAxis);

  const zoomed = (event: SVGElementZoomEvent) => {
    const newXScale = event.transform.rescaleX(xScale);
    const newYScale = event.transform.rescaleY(yScale);
    const zoomScale = event.transform.k;

    const { xAxis: newXAxis, yAxis: newYAxis } = prepareAxes({
      innerHeight,
      innerWidth,
      xScale: newXScale,
      yScale: newYScale,
      zoomScale,
    });

    xAxisLayer.call(newXAxis);
    yAxisLayer.call(newYAxis);

    renderDataPoints({
      dataLayer,
      innerHeight,
      innerWidth,
      points,
      showVolumes,
      theme,
      volumeScale,
      xScale: newXScale,
      yScale: newYScale,
      zoomScale,
    });

    renderRectangles({
      ugcRectLayer,
      rectangles,
      zoomScale,
      transform: event.transform,
    });
  };

  if (drawing) {
    disableZooming(svg);
    enableDrawing(svg, padding, dataActions.addRectangle);
  } else {
    disableDrawing(svg);
    enableZooming({
      svg,
      padding,
      width,
      height,
      onZoomed: zoomed,
    });
  }

  renderDataPoints({
    dataLayer,
    innerHeight,
    innerWidth,
    points,
    showVolumes,
    theme,
    volumeScale,
    xScale,
    yScale,
    zoomScale: 1,
  });

  renderRectangles({
    ugcRectLayer,
    rectangles,
    zoomScale: 1,
  });
}
