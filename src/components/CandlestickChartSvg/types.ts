import type {
  Axis,
  D3DragEvent,
  D3ZoomEvent,
  ScaleLinear,
  ScaleTime,
  Selection,
} from 'd3';

export type GElementSelection = Selection<SVGGElement, unknown, null, undefined>;
export type RectElementSelection = Selection<SVGRectElement, unknown, null, undefined>;
export type SVGElementSelection = Selection<SVGSVGElement, unknown, null, undefined>;
export type SVGElementDragEvent = D3DragEvent<SVGSVGElement, unknown, unknown>;
export type SVGElementZoomEvent = D3ZoomEvent<SVGSVGElement, unknown>;

export type DateAxis = Axis<Date>;
export type NumberAxis = Axis<number>;

export type DateScaleTime = ScaleTime<number, number>;
export type NumberScaleLinear = ScaleLinear<number, number>;
