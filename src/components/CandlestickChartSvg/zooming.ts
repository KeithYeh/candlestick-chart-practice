import * as d3 from 'd3';
import { SVGElementZoomEvent, SVGElementSelection } from './types';

type Options = {
  svg: SVGElementSelection;
  padding: number;
  width: number;
  height: number;
  onZoomed: (event: SVGElementZoomEvent) => void;
};

export function enableZooming(options: Options) {
  const {
    svg,
    padding,
    width,
    height,
    onZoomed,
  } = options;

  // Setup the zoom and pan function
  const zoom = d3
    .zoom<SVGSVGElement, unknown>()
    .scaleExtent([0.5, 5])
    .extent([[padding, 0], [width - padding, height]])
    .translateExtent([[padding, 0], [width - padding, Infinity]])
    .on('zoom', onZoomed);

  svg
    .call(zoom)
    .transition()
    .duration(500);
}

export function disableZooming(svg: SVGElementSelection) {
  svg.on('.zoom', null);
}
