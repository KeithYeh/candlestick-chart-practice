import type { ZoomTransform } from 'd3';
import { Rectangle } from '@app/models';
import { GElementSelection } from './types';

type Options = {
  ugcRectLayer: GElementSelection;
  rectangles: Rectangle[];
  zoomScale: number;
  transform?: ZoomTransform;
};

export function renderRectangles(options: Options) {
  const {
    ugcRectLayer,
    rectangles,
    zoomScale,
    transform,
  } = options;

  ugcRectLayer.selectAll('rect').remove();

  ugcRectLayer
    .selectAll('rect')
    .data(rectangles)
    .enter()
    .append('rect')
    .attr('class', 'ugc-rect')
    .attr('x', (r) => (transform ? transform.applyX(r.x) : r.x))
    .attr('y', (r) => (transform ? transform.applyY(r.y) : r.y))
    .attr('width', (r) => r.width * zoomScale)
    .attr('height', (r) => r.height * zoomScale);
}
