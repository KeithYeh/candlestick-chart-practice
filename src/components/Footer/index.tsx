import React from 'react';
import styled from 'styled-components';

const Container = styled.footer`
  display: flex;
  margin-top: 1rem;
  height: ${(props) => props.theme.footerHeight};
  align-items: center;
  justify-content: center;
  
  > a {
    text-decoration: none;
    
    &:hover {
      text-decoration: underline;
    }
  }
`;

export function Footer() {
  return (
    <Container>
      <a href="https://gitlab.com/KeithYeh/candlestick-chart-practice">© KeithYeh</a>
    </Container>
  );
}
