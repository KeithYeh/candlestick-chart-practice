// Seconds
export enum Interval {
  ONE_MINUTE      = 60,
  FIVE_MINUTES    = 300,
  FIFTEEN_MINUTES = 900,
  THIRTY_MINUTES  = 1800,
  AN_HOUR         = 3600,
  TWO_HOURS       = 7200,
  FOUR_HOURS      = 14400,
  ONE_DAY         = 86400,
  ONE_WEEK        = 604800,
  ONE_MONTH       = 2592000,
}
