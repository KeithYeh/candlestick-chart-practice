import { useGlobalStore } from '@app/context/GlobalStore';

export function useTheme() {
  return useGlobalStore().theme;
}
