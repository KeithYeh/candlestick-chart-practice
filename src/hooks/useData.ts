import { useGlobalStore } from '@app/context/GlobalStore';

export function useData() {
  return useGlobalStore().data;
}
