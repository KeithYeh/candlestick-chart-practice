import React from 'react';

import { Header } from '@app/components/Header';
import { CandlestickChartSvg } from '@app/components/CandlestickChartSvg';
import { ChartContainer } from '@app/components/ChartContainer';
import { Footer } from '@app/components/Footer';

export default function Home() {
  return (
    <>
      <Header />
      <ChartContainer>
        <CandlestickChartSvg />
      </ChartContainer>
      <Footer />
    </>
  );
}
