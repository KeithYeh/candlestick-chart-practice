import React, { useEffect } from 'react';
import Head from 'next/head';
import { ThemeProvider } from 'styled-components';
import 'normalize.css';

import { GlobalStore, useReducer } from '@app/context/GlobalStore';
import '@app/global.css';

import type { AppProps } from 'next/app';

function MyApp({ Component, pageProps }: AppProps) {
  const appReducer = useReducer();

  useEffect(() => {
    appReducer.data.actions.regeneratePoints();
  }, []); // eslint-disable-line

  // Attribution
  useEffect(() => {
    console.log('Favicon made by https://www.flaticon.com/authors/ultimatearm'); // eslint-disable-line
    console.log('Svg Icons made by https://fontawesome.com/'); // eslint-disable-line
  }, []);

  return (
    <>
      <Head>
        <title>Candlestick Chart Practice</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
      <ThemeProvider theme={appReducer.theme.state}>
        <GlobalStore.Provider value={appReducer}>
          <Component {...pageProps} />
        </GlobalStore.Provider>
      </ThemeProvider>
    </>
  );
}

export default MyApp;
