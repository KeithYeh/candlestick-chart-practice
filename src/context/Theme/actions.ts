import { Dispatch } from 'react';

export enum ActionType {
  UPDATE_BEAR_COLOR,
  UPDATE_BULL_COLOR,
}

export type Action = {
  type: ActionType;
  bearColor?: string;
  bullColor?: string;
};

export type Actions = {
  updateBearColor(color: string): void;
  updateBullColor(color: string): void;
};

export function createActions(dispatch: Dispatch<Action>): Actions {
  return {
    updateBearColor(bearColor: string) {
      dispatch({ type: ActionType.UPDATE_BEAR_COLOR, bearColor });
    },

    updateBullColor(bullColor: string) {
      dispatch({ type: ActionType.UPDATE_BULL_COLOR, bullColor });
    },
  };
}
