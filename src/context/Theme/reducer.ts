import { Action, ActionType } from './actions';
import { initialState, State } from './state';

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case ActionType.UPDATE_BEAR_COLOR:
      return {
        ...state,
        bearColor: action.bearColor ?? initialState.bearColor,
      };
    case ActionType.UPDATE_BULL_COLOR:
      return {
        ...state,
        bullColor: action.bullColor ?? initialState.bullColor,
      };
    default:
      return state;
  }
}
