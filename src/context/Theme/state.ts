export type State = {
  bearColor: string;
  bullColor: string;
  footerHeight: string;
  headerHeight: string;
};

export const initialState: State = {
  bearColor: '#e63946',
  bullColor: '#46e637',
  footerHeight: '2rem',
  headerHeight: '4rem',
};
