import { useReducer as useReactReducer } from 'react';
import { Actions, createActions } from './actions';
import { reducer } from './reducer';
import { initialState, State } from './state';

export function useReducer(): [State, Actions] {
  const [state, dispatch] = useReactReducer(reducer, initialState);

  return [state, createActions(dispatch)];
}
