import { Action, ActionType } from './actions';
import { initialState, State } from './state';

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case ActionType.ADD_RECTANGLE:
      return {
        ...state,
        rectangles: action.rectangle ? [...state.rectangles, action.rectangle] : state.rectangles,
      };
    case ActionType.UPDATE_DRAWING:
      return {
        ...state,
        drawing: action.drawing ?? false,
      };
    case ActionType.UPDATE_INTERVAL:
      return {
        ...state,
        interval: action.interval ?? initialState.interval,
      };
    case ActionType.UPDATE_POINTS:
      return {
        ...state,
        points: action.points ?? [],
      };
    case ActionType.UPDATE_RECTANGLES:
      return {
        ...state,
        rectangles: action.rectangles ?? [],
      };
    case ActionType.UPDATE_SHOW_VOLUMES:
      return {
        ...state,
        showVolumes: action.showVolumes ?? false,
      };
    default:
      return state;
  }
}
