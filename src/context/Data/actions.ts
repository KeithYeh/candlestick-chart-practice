import { Dispatch } from 'react';
import { Interval, Point, Rectangle } from '@app/models';
import { generatePoints, Options } from './helpers';

export enum ActionType {
  ADD_RECTANGLE,
  UPDATE_DRAWING,
  UPDATE_INTERVAL,
  UPDATE_POINTS,
  UPDATE_RECTANGLES,
  UPDATE_SHOW_VOLUMES,
}

export type Action = {
  type: ActionType;
  drawing?: boolean;
  interval?: Interval;
  points?: Point[];
  rectangle?: Rectangle;
  rectangles?: Rectangle[];
  showVolumes?: boolean;
};

export type Actions = {
  addRectangle(rectangle: Rectangle): void;
  regeneratePoints(options?: Options): void;
  updateDrawing(drawing: boolean): void;
  updateInterval(interval: Interval): void;
  updatePoints(points: Point[]): void;
  updateRectangles(rectangles: Rectangle[]): void;
  updateShowVolumes(showVolumes: boolean): void;
};

export function createActions(dispatch: Dispatch<Action>): Actions {
  return {
    addRectangle(rectangle: Rectangle) {
      dispatch({ type: ActionType.ADD_RECTANGLE, rectangle });
    },

    regeneratePoints(options?: Options) {
      const points = generatePoints(options ?? {});

      dispatch({ type: ActionType.UPDATE_POINTS, points });
    },

    updateDrawing(drawing: boolean) {
      dispatch({ type: ActionType.UPDATE_DRAWING, drawing });
    },

    updateInterval(interval: Interval) {
      dispatch({ type: ActionType.UPDATE_INTERVAL, interval });
    },

    updatePoints(points: Point[]) {
      dispatch({ type: ActionType.UPDATE_POINTS, points });
    },

    updateRectangles(rectangles: Rectangle[]) {
      dispatch({ type: ActionType.UPDATE_RECTANGLES, rectangles });
    },

    updateShowVolumes(showVolumes: boolean) {
      dispatch({ type: ActionType.UPDATE_SHOW_VOLUMES, showVolumes });
    },
  };
}
