import { random } from 'lodash';

type Options = {
  basePrice: number;
  priceLimit: number;
  resistance: number;
  support: number;
};

export function adjustPrice(options: Options): number {
  const {
    basePrice,
    priceLimit,
    resistance,
    support,
  } = options;

  const adjustment = basePrice * random(0, priceLimit, true);

  if (basePrice < support) {
    return basePrice + adjustment;
  }

  if (basePrice > resistance) {
    return basePrice - adjustment;
  }

  return random(0, 1)
    ? basePrice + adjustment
    : basePrice - adjustment;
}
