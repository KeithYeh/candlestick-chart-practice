import { subSeconds } from 'date-fns';
import { random } from 'lodash';
import { Interval, Point } from '@app/models';

import { generatePoint } from './generatePoint';

export type Options = {
  baseDate?: Date;
  interval?: Interval;
  numOfPoints?: number;
  priceLimit?: number;
  resistance?: number;
  support?: number;
};

export function generatePoints(options: Options): Point[] {
  // TODO Add parameters validations
  const {
    baseDate = new Date(),
    interval = Interval.ONE_DAY,
    numOfPoints = 200,
    priceLimit = 0.3,
    resistance = 1200,
    support = 200,
  } = options;

  const points: Point[] = Array(numOfPoints);

  points[numOfPoints - 1] = generatePoint({
    nextOpenValue: random(support, resistance, true),
    priceLimit,
    resistance,
    support,
    time: new Date(baseDate),
  });

  for (let i = numOfPoints - 2; i >= 0; i--) {
    const nextPoint = points[i + 1];

    points[i] = generatePoint({
      nextOpenValue: nextPoint.open,
      priceLimit,
      resistance,
      support,
      // Include weekend for now
      time: subSeconds(nextPoint.time, interval),
    });
  }

  return points;
}
