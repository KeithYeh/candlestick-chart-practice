import { Point } from '@app/models';
import { random } from 'lodash';
import { adjustPrice } from './adjustPrice';

type Options = {
  nextOpenValue: number;
  priceLimit: number;
  resistance: number;
  support: number;
  time: Date;
};

export function generatePoint(options: Options): Point {
  const {
    nextOpenValue,
    priceLimit,
    resistance,
    support,
    time,
  } = options;

  const close = adjustPrice({
    basePrice: nextOpenValue,
    priceLimit: priceLimit / 3,
    resistance,
    support,
  });

  const open = adjustPrice({
    basePrice: close,
    priceLimit,
    resistance,
    support,
  });

  const high = Math.max(open, close) * (1 + random(0, priceLimit, true));
  const low = Math.min(open, close) * (1 - random(0, priceLimit, true));
  const volume = random(100, 2000);

  return {
    open,
    high,
    low,
    close,
    volume,
    time,
  };
}
