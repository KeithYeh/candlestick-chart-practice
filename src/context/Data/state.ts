import {
  Interval,
  Point,
  Rectangle,
} from '@app/models';

export type State = {
  drawing: boolean;
  interval: Interval;
  points: Point[];
  rectangles: Rectangle[];
  showVolumes: boolean;
};

export const initialState: State = {
  drawing: false,
  interval: Interval.ONE_DAY,
  points: [],
  rectangles: [],
  showVolumes: false,
};
