import {
  Actions as DataActions,
  State as DataState,
  useReducer as useDataReducer,
} from '../Data';
import {
  Actions as ThemeActions,
  State as ThemeState,
  useReducer as useThemeReducer,
} from '../Theme';

export type AppReducer = {
  data: { actions: DataActions; state: DataState };
  theme: { actions: ThemeActions; state: ThemeState };
};

const combineReducers = (reducers: Record<string, () => any[]>) => {
  const appReducer: any = {};

  Object.entries(reducers).forEach(([key, reducer]) => {
    const [state, actions] = reducer();
    appReducer[key] = { state, actions };
  });

  return appReducer;
};

export function useReducer(): AppReducer {
  return combineReducers({
    data: useDataReducer,
    theme: useThemeReducer,
  });
}
