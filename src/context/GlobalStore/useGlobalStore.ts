import { createContext, useContext } from 'react';
import { AppReducer } from './useReducer';

export const GlobalStore = createContext<AppReducer>(undefined as unknown as AppReducer);

export const useGlobalStore = () => useContext(GlobalStore);
